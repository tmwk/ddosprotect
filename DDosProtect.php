<?php
/**
 * Created by PhpStorm.
 * User: Mario Figueroa
 * Email: mfigueroa@tmwk.cl
 * Date: 27/12/2017
 * Time: 22:09
 */

class DDosProtect
{
    public function logIp()
    {
        $date = new \DateTime('now');
        $ip   = $_SERVER['REMOTE_ADDR'];
        $file = fopen('ip.log', 'a');
        fwrite($file, "[" . $date->format('d-m-Y H:i:s') . "] IP: $ip \r\n");
        fclose($file);
    }

    /**
     * @param array $ips
     */
    public function denyIps(array $ips)
    {
        $current_ip = $_SERVER['REMOTE_ADDR'];
        if (in_array($current_ip, $ips)) {
            exit('Denied access for this ip');
        }
    }

    /**
     * @param array $ranges
     */
    public function denyIpsForRange(array $ranges)
    {
        $current_ip = $_SERVER['REMOTE_ADDR'];

        foreach ($ranges as $start => $end) {
            if ((ip2long($start) <= ip2long($current_ip) && ip2long($end) >= ip2long($current_ip))) {
                exit('Denied access for this ip');
            }
        }
    }

    /**
     * @param array $ips
     */
    public function allowIps(array $ips)
    {
        $current_ip = $_SERVER['REMOTE_ADDR'];
        if (!in_array($current_ip, $ips)) {
            exit('Denied access for this ip');
        }
    }

    /**
     * @param array $ranges
     */
    public function allowIpsForRange(array $ranges)
    {
        $current_ip = $_SERVER['REMOTE_ADDR'];

        foreach ($ranges as $start => $end) {
            if (ip2long($start) <= ip2long($current_ip) && ip2long($end) >= ip2long($current_ip)) {

            }else{
                exit('Denied access for this ip');
            }
        }
    }

}

