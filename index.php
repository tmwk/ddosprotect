<?php
/**
 * Created by PhpStorm.
 * User: Mario Figueroa
 * Email: mfigueroa@tmwk.cl
 * Date: 27/12/2017
 * Time: 22:09
 */

require 'DDosProtect.php';

$util = new DDosProtect();

$util->logIp();

//$util->denyIps(array());

$util->allowIpsForRange(array(
    '127.0.0.0' => '127.0.0.3'
));

echo 'OK';